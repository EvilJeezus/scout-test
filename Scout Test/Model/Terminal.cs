﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Scout_Test.Annotations;

namespace Scout_Test.Model
{
    public class Terminal : INotifyPropertyChanged
    {
        #region fields

       
        private string protocol;
        private string serialId;
        private string simNumber;
        private DateTime connectionTime;

        #endregion


        #region constructors

        public Terminal(string protocol, string serialId)
        {
            Sensors = new SynchronizedDictionary();
            Protocol = protocol;
            SerialId = serialId;
        }

        #endregion


        #region Terminal properties with notification

        public string Protocol
        {
            get { return protocol; }
            set
            {
                if (value == protocol)
                {
                    return;
                }
                protocol = value;
                OnPropertyChanged();
            }
        }

        public string SerialId
        {
            get { return serialId; }
            set
            {
                if (value == serialId)
                {
                    return;
                }
                serialId = value;
                OnPropertyChanged();
            }
        }

        public string SimNumber
        {
            get { return simNumber; }
            set
            {
                if (value == simNumber)
                {
                    return;
                }
                simNumber = value;
                OnPropertyChanged();
            }
        }

        public DateTime ConnectionTime
        {
            get { return connectionTime; }
            set
            {
                if (value.Equals(connectionTime))
                {
                    return;
                }
                connectionTime = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(ConnectionTimeString));
            }
        }

        public string ConnectionTimeString
        {
            get
            {
                if (ConnectionTime.Equals(new DateTime()))
                {
                    return "n/a";
                }
                else
                {
                    return ConnectionTime.ToString("G");
                }
            }
        }

        #endregion


        #region Sensors
        public SynchronizedDictionary Sensors;

        public ObservableCollection<string> SensorValues { get { return Sensors.AllValues; } }

        public void UpdateSensorValue(string sensorType, string value)
        {
            Sensors.Add(sensorType,value);
        }

        #endregion


        #region Equality members
        /// <summary>
        /// Определяет совпадают ли поля <see cref="Terminal.Protocol"/> и <see cref="Terminal.SerialId"/> у операндов
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(Terminal a,Terminal b)
        {
            if (ReferenceEquals(a, null) || ReferenceEquals(b, null)) return false;
            return  (a.Protocol.Equals(b.Protocol) && a.SerialId.Equals(b.SerialId));
        }

        public static bool operator !=(Terminal a, Terminal b)
        {
            return !(a == b);
        }


        protected bool Equals(Terminal other)
        {
            return string.Equals(Protocol, other.Protocol, StringComparison.InvariantCulture) && string.Equals(SerialId, other.SerialId, StringComparison.InvariantCulture);
        }

        /// <summary>
        /// Определяет, равен ли заданный объект текущему объекту.
        /// </summary>
        /// <returns>
        /// true, если указанный объект равен текущему объекту; в противном случае — false.
        /// </returns>
        /// <param name="obj">Объект, который требуется сравнить с текущим объектом. </param>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != GetType())
            {
                return false;
            }
            return Equals((Terminal)obj);
        }

        /// <summary>
        /// Служит хэш-функцией по умолчанию. 
        /// </summary>
        /// <returns>
        /// Хэш-код для текущего объекта.
        /// </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return ((Protocol != null ? StringComparer.InvariantCulture.GetHashCode(Protocol) : 0) * 397) ^ (SerialId != null ? StringComparer.InvariantCulture.GetHashCode(SerialId) : 0);
            }
        }

        #endregion

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
