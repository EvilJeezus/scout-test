﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Scout_Test.Annotations;
using Scout_Test.Utils;

namespace Scout_Test.Model
{
    public class SynchronizedDictionary : Dictionary<string, string>,INotifyCollectionChanged,INotifyPropertyChanged
    {
        #region fields

        private delegate void DictionaryEventHandler(object sender, DictionaryEventArgs args);

        private readonly ObservableCollection<string> allValues;
        private static string defaultValue = "-";

        #endregion


        #region properties

        public static ObservableCollection<string> GobalKeys { get; }

        public ObservableCollection<string> AllValues
        {
            get
            {
                allValues.Clear();
                allValues.AddRange(Values);
                return allValues;
            }
        }

        #endregion


        #region constructors

        static SynchronizedDictionary()
        {
            GobalKeys = new ObservableCollection<string>();
        }

        public SynchronizedDictionary()
        {
            OnChanged += OnChangedHandler;
            foreach (string key in GobalKeys)
            {
                base.Add(key, defaultValue);
            }
            allValues = new ObservableCollection<string>(Values);
        }

        #endregion


        #region methods

        private void OnChangedHandler(object sender, DictionaryEventArgs e)
        {
            if (sender != this)
                switch (e.Operation)
                {
                    case DictionaryOperation.Add:
                        if (!ContainsKey(e.Key))
                        {
                            base.Add(e.Key, defaultValue);

                        }
                        break;
                    case DictionaryOperation.Remove:
                        break;
                }
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public void Unsubscribe()
        {
            OnChanged -= OnChangedHandler;
        }

        public new void Add(string key, string value)
        {
            if (!ContainsKey(key))
            {
                base.Add(key, value);
                GobalKeys.Add(key);
            }
            else
            {
                base[key] = value;
            }
            OnChanged?.Invoke(this, new DictionaryEventArgs(key, value, DictionaryOperation.Add));
        }

        private static event DictionaryEventHandler OnChanged;

        #endregion


        #region INotifyCollectionChanged implementation

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        #endregion

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }

    internal class DictionaryEventArgs : EventArgs
    {
        public readonly string Key;
        public readonly string Value;
        public readonly DictionaryOperation Operation;

        internal DictionaryEventArgs(string key, string value, DictionaryOperation operation)
        {
            Key = key;
            Value = value;
            Operation = operation;
        }
    }

    enum DictionaryOperation
    {
        Add, Remove
    }
}
