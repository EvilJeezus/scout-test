﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Scout_Test.Utils;

namespace Scout_Test.Model
{
    class DataExtractor
    {
        #region fields

        public ObservableCollection<Terminal> Terminals;
        private readonly List<Terminal> originalTerminals;

        #endregion


        #region constructors

        public DataExtractor()
        {
            originalTerminals = new List<Terminal>();
            Terminals = new ObservableCollection<Terminal>();
        }

        #endregion

        #region Data management

        /// <summary>
        /// Опустошает текущий набор данных экземпляра <see cref="DataExtractor"/>
        /// </summary>
        public void ClearData()
        {
            SynchronizedDictionary.GobalKeys.Clear();
            originalTerminals.Clear();
        }

        /// <summary>
        /// Загружает данные из xml-файла в текущий набор данных экземпляра <see cref="DataExtractor"/>
        /// </summary>
        /// <param name="fileName">Полный URI xml-файла</param>
        public void AddTerminals(string fileName)
        {
            try
            {
                XDocument document = XDocument.Load(XmlReader.Create(fileName));
                if (document.Root == null)
                {
                    return;
                }
                List<Terminal> terminals = document.Root.Descendants("terminal").Select(ParseTerminal).ToList();

                originalTerminals.AddRange(terminals);
                originalTerminals.Sort((a, b) => a.ConnectionTime.CompareTo(b.ConnectionTime));
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Возвращает перечисление <see cref="Terminal"/> текущего набора данных экземпляра <see cref="DataExtractor"/> с актуальными данными, без повторений
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Terminal> GetMergedTerminals()
        {
            List<Terminal> terminals = new List<Terminal>();
            foreach (Terminal terminal in originalTerminals)
            {
                MergeTerminals(terminals, terminal);
            }
            return terminals;
        }

        /// <summary>
        /// Обновляет коллекцию <paramref name="terminals"/>, добавляя в неё <paramref name="terminal"/>, либо модифицирует имеющийся в ней <see cref="Terminal"/>, если его свойства <see cref="Terminal.Protocol"/> и <see cref="Terminal.SerialId"/> совпадают с соответвующими свойствами <paramref name="terminal"/>
        /// </summary>
        /// <param name="terminals"></param>
        /// <param name="terminal"></param>
        private static void MergeTerminals(ICollection<Terminal> terminals, Terminal terminal)
        {
            if (terminals.Count == 0)
            {
                terminals.Add(terminal);
                return;
            }
            if (terminals.All(a => a != terminal))
            {
                terminals.Add(terminal);
                return;
            }
            Terminal conflict = terminals.First(a => a == terminal);
            bool lastIsNewer = conflict.ConnectionTime < terminal.ConnectionTime;
            foreach (KeyValuePair<string, string> pair in terminal.Sensors)
            {
                string conflictValue = conflict.Sensors[pair.Key];
                if (conflictValue != pair.Value && lastIsNewer)
                {
                    conflict.Sensors.Add(pair.Key, pair.Value);
                }
            }
            if (lastIsNewer) conflict.ConnectionTime = terminal.ConnectionTime;
        }

        /// <summary>
        /// Конструирует объект класса <see cref="Terminal"/> на основе <see cref="XElement"/> <paramref name="terminalElement"/>
        /// </summary>
        /// <param name="terminalElement"></param>
        /// <returns>Объект <see cref="Terminal"/> сконструированный на основе <paramref name="terminalElement"/></returns>
        private static Terminal ParseTerminal(XElement terminalElement)
        {
            Terminal terminal = new Terminal(terminalElement.Attribute("protocol").Value, terminalElement.Attribute("serialId").Value);
            XAttribute connectionTimeAttribute = terminalElement.Attribute("connectionTime");
            terminal.SimNumber = terminalElement.Attribute("simNumber") != null ? terminalElement.Attribute("simNumber").Value : "-";
            if (connectionTimeAttribute != null)
            {
                int[] splitDate = connectionTimeAttribute.Value.Split('.').Select(Int32.Parse).ToArray();
                terminal.ConnectionTime = new DateTime(splitDate[2], splitDate[1], splitDate[0], splitDate[3], splitDate[4], splitDate[5]);
            }
            else
            {
                terminal.ConnectionTime = new DateTime();
            }
            IEnumerable<XElement> sensorElements = terminalElement.Descendants("sensor");
            foreach (XElement sensor in sensorElements)
            {
                string type = sensor.Attribute("type").Value;
                string value = sensor.Attribute("value").Value;
                terminal.UpdateSensorValue(type, value);
            }
            return terminal;
        }

        /// <summary>
        /// Обновляет коллекцию <see cref="DataExtractor.Terminals"/> до актуального состояния
        /// </summary>
        public void UpdateTerminals()
        {
            Terminals.Clear();
            Terminals.AddRange(GetMergedTerminals());
        }

        #endregion

    }
}
