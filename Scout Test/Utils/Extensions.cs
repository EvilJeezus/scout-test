﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scout_Test.Utils
{
    static class Extensions
    {
        public static bool IsNullOrEmpty(this string self)
        {
            return string.IsNullOrEmpty(self) && string.IsNullOrWhiteSpace(self);
        }

        public static void AddRange<T>(this ObservableCollection<T> self, IEnumerable<T> items)
        {
            foreach (T item in items)
            {
                self.Add(item);
            }
        }
    }
}
