﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Microsoft.Win32;
using Scout_Test.Annotations;
using Scout_Test.Model;
using Scout_Test.Utils;
using Scout_Test.ViewModel;

namespace Scout_Test
{
    public class MainWindowViewModel
    {
        #region ui binding

        public ObservableCollection<Terminal> Terminals { get; set; }
        public ObservableCollection<string> SensorTypes { get; set; }
        public ICommand ShowAddDataDialogCommand { get; set; }
        public ICommand ClearDataCommand { get; set; }
        public ICommand ExitCommand { get; set; }

        #endregion


        #region file load fields

        private OpenFileDialog openFileDialog;
        private readonly DataExtractor dataExtractor;
        private readonly Queue<string> fileLoadingQueue;
        private Task fileLoadingTask;
        private SynchronizationContext mainContext;

        #endregion


        #region constructors

        public MainWindowViewModel()
        {
            Terminals = new ObservableCollection<Terminal>();
            SensorTypes = new ObservableCollection<string>();
            dataExtractor = new DataExtractor();
            ShowAddDataDialogCommand = new RelayCommand(ShowAddDataDialog);
            ExitCommand = new RelayCommand(Application.Current.Shutdown);
            ClearDataCommand = new RelayCommand(ClearData);
            fileLoadingQueue = new Queue<string>();
            mainContext = SynchronizationContext.Current;
        }

        #endregion


        #region file load methods

        /// <summary>
        /// Отображает диалог выбора файла, по окончании выбора начинает асинхронную загрузку данных в текущий <see cref="DataExtractor"/>
        /// </summary>
        public void ShowAddDataDialog()
        {
            openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = false;
            openFileDialog.CheckFileExists = true;
            openFileDialog.FileOk += Openfile_FileOk;
            openFileDialog.Filter = "Data Sources (*.txt, *.xml)|*.txt*;*.xml|All Files|*.*";
            openFileDialog.ShowDialog();
        }


        /// <summary>
        /// Очищает данные в коллекциях и <see cref="DataExtractor"/>
        /// </summary>
        public void ClearData()
        {
            SensorTypes.Clear();
            Terminals.Clear();
            dataExtractor.ClearData();
        }

        /// <summary>
        /// Вызывается при нажатии кнопки подтверждения в окне выбора файла, начинает асинхронную загрузку файла
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Openfile_FileOk(object sender, CancelEventArgs e)
        {
            fileLoadingQueue.Enqueue(openFileDialog.FileName);
            if (fileLoadingTask != null)
            {
                return;
            }
            fileLoadingTask = new Task(LoadNextFile);
            fileLoadingTask.ContinueWith(OnLoadCompleted);
            fileLoadingTask.Start();

        }

        /// <summary>
        /// Загружает данные в текущий <see cref="DataExtractor"/> и обновляет свойство <see cref="DataExtractor.Terminals"/>
        /// </summary>
        private void LoadNextFile()
        {
            while (fileLoadingQueue.Count == 0) Thread.Sleep(100);
            dataExtractor.AddTerminals(fileLoadingQueue.Dequeue());
            dataExtractor.UpdateTerminals();
        }

        /// <summary>
        /// Вызывается после завершения обработки текущего файла, вызывает обновление коллекций связанных с View
        /// </summary>
        /// <param name="loadTask"></param>
        private void OnLoadCompleted(Task loadTask)
        {
            mainContext.Post(UpdateView, null);
            fileLoadingTask = new Task(LoadNextFile);
            fileLoadingTask.ContinueWith(OnLoadCompleted);
            fileLoadingTask.Start();
        }

        /// <summary>
        /// Обновляет коллекции связанные с View
        /// </summary>
        /// <param name="state"></param>
        private void UpdateView(object state)
        {
            SensorTypes.Clear();
            Terminals.Clear();
            Terminals.AddRange(dataExtractor.Terminals);
            if (Terminals.Count > 0) SensorTypes.AddRange(Terminals[0].Sensors.Keys);
        }

        #endregion

    }
}