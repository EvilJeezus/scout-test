using System;
using System.Diagnostics;
using System.Windows.Input;

namespace Scout_Test.ViewModel
{
    public class RelayCommand : ICommand
    {
        #region Fields

        readonly Action execute;
        public bool canExecute { get; set; }

        #endregion

        #region Constructors

        public RelayCommand(Action execute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            this.execute = execute;
            canExecute = true;
        }
        #endregion

        #region ICommand Members

        public bool CanExecute(object parameter)
        {
            return canExecute;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object obj)
        {
            execute();
        }

        #endregion
    }
}
